# Producement Libraries

This project contains a bunch of open source helper libraries that can be used inside or outside
Producement.

## Usage

Import projects from Maven Central:

```kotlin
  dependencies {
    implementation("com.producement:time:0.0.1")
  }
```

## Projects

- `time` - Timing and clock related helpers.
- `test-annotations` - Meta-annotations that help with testing Spring Boot web applications.
- `test-architecture` - Static code analysis rules for Java/Kotlin projects.

## Publishing

Projects are published automatically by Gitlab.
