package com.producement.time

import java.util.concurrent.TimeUnit

class Sleeper {
  fun sleep(value: Long, unit: TimeUnit) {
    Thread.sleep(unit.toMillis(value))
  }
}
