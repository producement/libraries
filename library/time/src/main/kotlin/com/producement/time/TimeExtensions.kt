package com.producement.time

import java.sql.Timestamp
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime

fun Instant?.toTimestamp() = this?.let { Timestamp.from(this) }
fun String?.toLocalTime() = if (this == null) null else LocalTime.parse(this)
fun String?.toLocalDate() = if (this == null) null else LocalDate.parse(this)
fun String?.toLocalDateTime() = if (this == null) null else LocalDateTime.parse(this)
