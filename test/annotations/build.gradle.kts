dependencies {
  compileOnly("org.springframework.boot:spring-boot-starter-test")
  compileOnly("org.springframework:spring-jdbc")
  compileOnly("org.testcontainers:testcontainers:1.16.0")
  compileOnly("org.springframework.boot:spring-boot-starter-data-jdbc")
  compileOnly(project(":time"))
}
