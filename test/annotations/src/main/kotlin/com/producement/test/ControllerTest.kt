package com.producement.test

import com.producement.test.time.ClockTestExecutionListener
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.TestInstance
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.core.annotation.AliasFor
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestExecutionListeners
import kotlin.reflect.KClass

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FILE)
@WebMvcTest
@AutoConfigureJson
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestExecutionListeners(
  listeners = [ClockTestExecutionListener::class],
  mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
)
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
@Tag("unit")
annotation class ControllerTest(
  @get:AliasFor(value = "controllers", annotation = WebMvcTest::class) val controller: Array<KClass<*>> = []
)
