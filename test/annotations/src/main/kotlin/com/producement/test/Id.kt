package com.producement.test

import java.util.UUID
import kotlin.random.Random
import kotlin.reflect.full.instanceParameter
import kotlin.reflect.full.memberFunctions

private val idCache = hashSetOf<Long>()

fun randomId(): Long {
  val id = Random.nextLong(0, 10000000)
  return if (idCache.add(id)) {
    id
  } else randomId()
}

fun randomUUID() = UUID.randomUUID()

fun randomString() = randomUUID().toString()

// Returns a new instance of a data class where id is null
@Suppress("UNCHECKED_CAST")
val <T> T.asNew: T
  get() = this.also {
    val copy = it!!::class.memberFunctions.first { it.name == "copy" }
    val idParam = copy.parameters.first { it.name == "id" }
    return copy.callBy(mapOf(copy.instanceParameter!! to this, idParam to null)) as T
  }
