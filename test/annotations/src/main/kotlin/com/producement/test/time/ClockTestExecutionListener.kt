package com.producement.test.time

import com.producement.time.ClockHolder
import com.producement.time.ClockHolder.timeZone
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.springframework.test.context.TestContext
import org.springframework.test.context.support.AbstractTestExecutionListener
import java.time.Clock

class ClockTestExecutionListener : AbstractTestExecutionListener(), BeforeEachCallback, AfterEachCallback {

  override fun beforeEach(context: ExtensionContext) {
    ClockHolder.clockImpl = fixedClock
  }

  override fun afterEach(context: ExtensionContext) {
    ClockHolder.clockImpl = Clock.system(timeZone)
  }

  override fun beforeTestMethod(testContext: TestContext) {
    ClockHolder.clockImpl = fixedClock
  }

  override fun afterTestMethod(testContext: TestContext) {
    ClockHolder.clockImpl = Clock.system(timeZone)
  }
}
