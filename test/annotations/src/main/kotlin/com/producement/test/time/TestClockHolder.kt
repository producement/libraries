package com.producement.test.time

import com.producement.time.ClockHolder
import java.time.Clock
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.random.Random

val now: Instant = Instant.EPOCH.plus(365 * 50L, ChronoUnit.DAYS)
  .plusSeconds(Random.nextLong(0, 500)).truncatedTo(ChronoUnit.SECONDS)
val fixedClock: Clock = Clock.fixed(now, ClockHolder.timeZone)
