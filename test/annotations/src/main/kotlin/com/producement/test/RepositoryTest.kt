package com.producement.test

import com.producement.test.fixture.FixtureAnnotationTestExecutionListener
import com.producement.test.time.ClockTestExecutionListener
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Tags
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
import kotlin.annotation.AnnotationRetention.RUNTIME
import kotlin.annotation.AnnotationTarget.CLASS

@Target(CLASS)
@Retention(RUNTIME)
@TestInstance(Lifecycle.PER_CLASS)
@DataJdbcTest(
  includeFilters = [
    ComponentScan.Filter(
      type = FilterType.ASSIGNABLE_TYPE,
      classes = [AbstractJdbcConfiguration::class]
    )
  ]
)
@AutoConfigureTestDatabase(replace = NONE)
@AutoConfigureJsonTesters
@ContextConfiguration(
  initializers = [DockerContainerInitializer::class]
)
@TestExecutionListeners(
  listeners = [
    FixtureAnnotationTestExecutionListener::class, ClockTestExecutionListener::class,
  ],
  mergeMode = MERGE_WITH_DEFAULTS
)
@Tags(value = [Tag("repository"), Tag("integration")])
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
annotation class RepositoryTest
