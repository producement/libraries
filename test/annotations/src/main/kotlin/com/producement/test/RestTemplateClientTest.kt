package com.producement.test

import com.producement.test.time.ClockTestExecutionListener
import org.junit.jupiter.api.Tag
import org.springframework.boot.test.autoconfigure.web.client.RestClientTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Import
import org.springframework.core.annotation.AliasFor
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestExecutionListeners
import kotlin.reflect.KClass

@RestClientTest
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
@Tag("unit")
@ActiveProfiles("test")
@TestExecutionListeners(
  listeners = [
    ClockTestExecutionListener::class,
  ],
  mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
)
annotation class RestTemplateClientTest(
  @get:AliasFor(annotation = RestClientTest::class, value = "value") val client: Array<KClass<*>> = []
)
