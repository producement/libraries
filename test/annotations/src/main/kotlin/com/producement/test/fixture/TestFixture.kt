package com.producement.test.fixture

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate

interface TestFixture {
  fun apply(jdbcTemplate: NamedParameterJdbcTemplate)
  fun cleanup(jdbcTemplate: NamedParameterJdbcTemplate) {}
}
