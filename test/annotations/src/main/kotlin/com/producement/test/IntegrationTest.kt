package com.producement.test

import com.producement.test.fixture.FixtureAnnotationTestExecutionListener
import com.producement.test.time.ClockTestExecutionListener
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.TestInstance
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.TestConstructor
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.TestPropertySource

@ContextConfiguration(initializers = [DockerContainerInitializer::class])
@TestExecutionListeners(
  listeners = [
    FixtureAnnotationTestExecutionListener::class,
    ClockTestExecutionListener::class,
  ],
  mergeMode = TestExecutionListeners.MergeMode.MERGE_WITH_DEFAULTS
)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestConstructor(autowireMode = TestConstructor.AutowireMode.ALL)
@Tag("integration")
@TestPropertySource(properties = ["security.enabled=false"])
annotation class IntegrationTest
