val archUnitVersion = "0.22.0"

dependencies {
  implementation("com.tngtech.archunit:archunit-junit5:$archUnitVersion")
}
