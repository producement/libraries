# Architecture tests

Producement internal architecture and code rules enforcement.

## Usage

In your Gradle buildfile:

```kotlin
apply(from = "https://lib.producement.dev/architecture.gradle.kts")
```

Adds a task `archTest` which depends on `check` and will run all the JUnit tests in this project.

## Rules

- Fails when `Instant.now()` is called without a `Clock`
- Fails when `Date` constructor is used (instead of `Date.from(instant)`)
- Fails when a service class method calls multiple `Repository.save` methods without being
  transactional.
- Fails when standard streams are accessed
- Fails when field injection is used
- Fails when Java Util Logging is used
- Fails when Joda Time is used
- Fails when generic exceptions are thrown
