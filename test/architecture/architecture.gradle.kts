apply<JavaPlugin>()

repositories {
  mavenLocal()
  maven(url = "https://s01.oss.sonatype.org/content/repositories/releases/")
  mavenCentral()
}

val architectureTests: Configuration by configurations.creating

val architectureTestsVersion = "0.+"

dependencies {
  architectureTests("com.producement:test-architecture:$architectureTestsVersion") {
    isTransitive = false
  }
  add("testImplementation", "com.producement:test-architecture:$architectureTestsVersion")
}

tasks {
  val archTest by creating(Test::class) {
    useJUnitPlatform()

    testClassesDirs += zipTree(architectureTests.singleFile)
    maxHeapSize = "2G"

    filter {
      includeTestsMatching("com.producement.test.arch.*")
    }

    systemProperty("arch.basePackage", project.group)

    testLogging {
      events = setOf(
        org.gradle.api.tasks.testing.logging.TestLogEvent.STARTED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_OUT,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_ERROR
      )
      showExceptions = true
      showCauses = true
      showStackTraces = true
      exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }

  }
  named("check") {
    dependsOn(archTest)
  }
}
