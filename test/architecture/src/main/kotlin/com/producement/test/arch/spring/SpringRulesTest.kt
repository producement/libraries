package com.producement.test.arch.spring

import com.producement.test.arch.BasePackageLocationProvider
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noMethods

@AnalyzeClasses(locations = [BasePackageLocationProvider::class])
class SpringRulesTest {
  @ArchTest
  val nonTransactionalMethodModifyingMultipleResources: ArchRule =
    noMethods().should(callMultipleModifyingMethodsWhileNotBeingTransactional())
}
