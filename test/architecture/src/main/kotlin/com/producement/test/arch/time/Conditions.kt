package com.producement.test.arch.time

import com.tngtech.archunit.core.domain.JavaConstructorCall
import com.tngtech.archunit.core.domain.JavaMethod
import com.tngtech.archunit.core.domain.JavaMethodCall
import com.tngtech.archunit.lang.ArchCondition
import com.tngtech.archunit.lang.ConditionEvents
import com.tngtech.archunit.lang.SimpleConditionEvent
import java.time.temporal.Temporal
import java.util.*

fun useTemporalNowWithoutClock() = object : ArchCondition<JavaMethod>("Using Temporal without clock") {
  override fun check(item: JavaMethod, events: ConditionEvents) {
    if (callsTemporalMethodNowWithoutClock(item)) {
      events.add(SimpleConditionEvent(item, true, item.description))
    }
  }

  private fun callsTemporalMethodNowWithoutClock(item: JavaMethod): Boolean {
    return item.methodCallsFromSelf.any { call: JavaMethodCall ->
      call.targetOwner.isAssignableTo(Temporal::class.java)
        && call.target.name == "now"
        && call.target.parameterTypes.isEmpty()
    }
  }
}

fun useJavaDateWithoutInstant() = object : ArchCondition<JavaMethod>("Using Date without instant") {
  override fun check(item: JavaMethod, events: ConditionEvents) {
    if (callsDateConstructor(item)) {
      events.add(SimpleConditionEvent(item, true, item.description))
    }
  }

  private fun callsDateConstructor(item: JavaMethod): Boolean {
    return item.constructorCallsFromSelf.any { call: JavaConstructorCall ->
      call.targetOwner.isAssignableTo(Date::class.java)
    }
  }

}
