package com.producement.test.arch.time

import com.producement.test.arch.BasePackageLocationProvider
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noMethods

@AnalyzeClasses(locations = [BasePackageLocationProvider::class])
class TimeRulesTest {

  @ArchTest
  val temporalNowWithoutClockShouldNotBeUsed: ArchRule = noMethods().should(useTemporalNowWithoutClock())

  @ArchTest
  val dateShouldNotBeUsed: ArchRule = noMethods().should(useJavaDateWithoutInstant())
}
