package com.producement.test.arch.spring

import com.tngtech.archunit.core.domain.JavaMethod
import com.tngtech.archunit.core.domain.JavaMethodCall
import com.tngtech.archunit.core.domain.JavaModifier
import com.tngtech.archunit.lang.ArchCondition
import com.tngtech.archunit.lang.ConditionEvents
import com.tngtech.archunit.lang.SimpleConditionEvent

fun callMultipleModifyingMethodsWhileNotBeingTransactional(): ArchCondition<in JavaMethod> =
  object : ArchCondition<JavaMethod>("call multiple modifying methods, being non transactional") {
    override fun check(item: JavaMethod, events: ConditionEvents) {
      if (isNonTransactionalServiceMethod(item)) {
        val callsToRepositoryMethods = item.methodCallsFromSelf.count(modifyingRepositoryMethods)
        events.add(SimpleConditionEvent(item, callsToRepositoryMethods > 1, item.description))
      }
    }

    private fun isNonTransactionalServiceMethod(item: JavaMethod): Boolean {
      return item.owner.isAnnotatedWith("org.springframework.stereotype.Service") &&
        !item.isAnnotatedWith("org.springframework.transaction.annotation.Transactional") &&
        item.modifiers.contains(JavaModifier.PUBLIC)
    }

    private val modifyingRepositoryMethods = { call: JavaMethodCall ->
      call.targetOwner.isAnnotatedWith("org.springframework.stereotype.Repository") &&
        (call.target.isAnnotatedWith("org.springframework.data.jdbc.repository.query.Modifying") ||
          call.target.name.startsWith(
            "save"
          ))
    }
  }
