package com.producement.test.arch

import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.library.GeneralCodingRules

@AnalyzeClasses(locations = [BasePackageLocationProvider::class])
class GeneralRulesTest {

  @ArchTest
  val standardStreamsAccess: ArchRule = GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS

  @ArchTest
  val noFieldInjection: ArchRule = GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION

  @ArchTest
  val noJavaUtilLogging: ArchRule = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING

  @ArchTest
  val noJodaTime: ArchRule = GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME

  @ArchTest
  val noGenericExceptions: ArchRule = GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS
}
