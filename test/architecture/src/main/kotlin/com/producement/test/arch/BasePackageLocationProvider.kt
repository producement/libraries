package com.producement.test.arch

import com.tngtech.archunit.core.importer.Location
import com.tngtech.archunit.core.importer.Locations
import com.tngtech.archunit.junit.LocationProvider

class BasePackageLocationProvider : LocationProvider {
  override fun get(testClass: Class<*>): Set<Location> {
    val basePackage = System.getProperty("arch.basePackage")
    return Locations.ofPackage(basePackage)
  }
}
