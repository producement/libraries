package com.producement.test.arch.time

import com.tngtech.archunit.core.importer.ClassFileImporter
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noMethods
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import java.time.Clock
import java.time.Instant
import java.time.LocalDate
import java.util.*


class TimeRulesTest {

  @Test
  fun `fails when now() is called without a clock`() {
    val timeRule = noMethods().should(useTemporalNowWithoutClock())
    assertThatExceptionOfType(AssertionError::class.java).isThrownBy {
      timeRule.check(ClassFileImporter().importClasses(FailingTimeRule::class.java))
    }
  }

  class FailingTimeRule {
    fun useLocalDateNowWithoutClock() {
      LocalDate.now()
    }
  }

  @Test
  fun `does not fail when now() is called with a clock`() {
    val timeRule = noMethods().should(useTemporalNowWithoutClock())
    timeRule.check(ClassFileImporter().importClasses(PassingTimeRule::class.java))
  }

  class PassingTimeRule {

    fun useLocalDateNowWithClock() {
      LocalDate.now(Clock.systemUTC())
    }
  }

  @Test
  fun `fails when Date() is used`() {
    val dateRule = noMethods().should(useJavaDateWithoutInstant())
    assertThatExceptionOfType(AssertionError::class.java).isThrownBy {
      dateRule.check(ClassFileImporter().importClasses(FailingDateRule::class.java))
    }
  }

  class FailingDateRule {
    fun useJavaDate() {
      Date()
    }
  }

  @Test
  fun `does not fail when Date is instantiated from Instant`() {
    val dateRule = noMethods().should(useJavaDateWithoutInstant())
    dateRule.check(ClassFileImporter().importClasses(PassingDateRule::class.java))
  }

  class PassingDateRule {
    fun useJavaDate() {
      Date.from(Instant.now(Clock.systemUTC()))
    }
  }

}
