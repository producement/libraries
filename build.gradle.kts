import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.5.31"
  id("io.gitlab.arturbosch.detekt") version "1.17.1" apply false
}

group = "com.producement"
version = "0.0.1"

repositories {
  mavenCentral()
}

subprojects {
  apply(plugin = "org.jetbrains.kotlin.jvm")
  apply(plugin = "io.gitlab.arturbosch.detekt")
  apply<MavenPublishPlugin>()
  apply<SigningPlugin>()
  apply<JavaLibraryPlugin>()

  java {
    withSourcesJar()
    withJavadocJar()
  }

  repositories {
    mavenLocal()
    mavenCentral()
  }

  dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation(platform("org.springframework.boot:spring-boot-dependencies:2.5.5"))
    implementation(platform("org.junit:junit-bom:5.8.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
    testImplementation("org.assertj:assertj-core:3.21.0")
    testImplementation("org.slf4j:slf4j-simple:1.7.32")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
  }

  tasks.withType<KotlinCompile> {
    kotlinOptions {
      freeCompilerArgs = listOf("-Xjsr305=strict")
      jvmTarget = "16"
    }
  }

  tasks.withType<JavaCompile> {
    sourceCompatibility = "16"
    targetCompatibility = "16"
  }

  tasks.test {
    useJUnitPlatform()
    testLogging {
      events = setOf(
        org.gradle.api.tasks.testing.logging.TestLogEvent.STARTED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_OUT,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_ERROR
      )
      showExceptions = true
      showCauses = true
      showStackTraces = true
      exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
  }

  configure<PublishingExtension> {
    publications {
      create<MavenPublication>("mavenJava") {
        artifactId = (this@subprojects).name
        groupId = "com.producement"
        from(components["java"])
        pom {
          name.set((this@subprojects).property("fullName") as String)
          description.set((this@subprojects).property("description") as String)
          url.set("https://gitlab.com/producement/libraries")
          licenses {
            license {
              name.set("The MIT License")
              url.set("http://www.opensource.org/licenses/MIT")
            }
          }
          developers {
            developer {
              id.set("maido")
              name.set("Maido Käära")
              email.set("maido@producement.com")
            }
          }
          scm {
            connection.set("scm:git:https://gitlab.com/producement/libraries.git")
            developerConnection.set("scm:git:ssh://git@gitlab.com:producement/libraries.git")
            url.set("https://gitlab.com/producement/libraries")
          }
        }
      }
    }
    repositories {
      maven {
        credentials {
          username = findProperty("nexusUsername").toString()
          password = findProperty("nexusPassword").toString()
        }
        val releasesRepoUrl = uri("https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/")
        val snapshotsRepoUrl = uri("https://s01.oss.sonatype.org/content/repositories/snapshots/")
        url = if (version.toString()
            .endsWith("SNAPSHOT")
        ) snapshotsRepoUrl else releasesRepoUrl
      }
    }
  }
  configure<SigningExtension> {
    sign(the(PublishingExtension::class).publications)
  }
}
