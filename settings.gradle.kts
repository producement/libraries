import java.io.FileFilter

rootProject.name = "libraries"

files("test", "plugin").forEach { topDir ->
  topDir?.listFiles(FileFilter { it.isDirectory })?.forEach { projectDir ->
    include(":${topDir.name}-${projectDir.name}")
    project(":${topDir.name}-${projectDir.name}").projectDir = projectDir
  }
}

files("library").forEach { topDir ->
  topDir?.listFiles(FileFilter { it.isDirectory })?.forEach { projectDir ->
    include(":${projectDir.name}")
    project(":${projectDir.name}").projectDir = projectDir
  }
}
