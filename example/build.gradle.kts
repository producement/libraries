import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  kotlin("jvm") version "1.5.31"
}

apply(from = "../test/architecture/architecture.gradle.kts")

group = "com.producement"
version = "0.0.1"

repositories {
  mavenCentral()
}

tasks {
  test {
    useJUnitPlatform()
    testLogging {
      events = setOf(
        org.gradle.api.tasks.testing.logging.TestLogEvent.STARTED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.PASSED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.FAILED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.SKIPPED,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_OUT,
        org.gradle.api.tasks.testing.logging.TestLogEvent.STANDARD_ERROR
      )
      showExceptions = true
      showCauses = true
      showStackTraces = true
      exceptionFormat = org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
    }
  }

  withType<KotlinCompile> {
    kotlinOptions {
      freeCompilerArgs = listOf("-Xjsr305=strict")
      jvmTarget = "16"
    }
  }

  withType<JavaCompile> {
    sourceCompatibility = "16"
    targetCompatibility = "16"
  }
}

dependencies {
  testImplementation(platform("org.junit:junit-bom:5.8.1"))
  testImplementation("org.junit.jupiter:junit-jupiter")
  testImplementation("org.assertj:assertj-core:3.21.0")
  testImplementation("org.slf4j:slf4j-simple:1.7.32")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}
